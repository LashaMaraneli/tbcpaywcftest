﻿using ServiceLayer;
using SmsService;
using System;
using System.Threading.Tasks;

namespace Tbcpaywcftest
{
    class Program
    {
        public static void Main(string[] args)
        {
            TestClass test = new TestClass();
            var response = test.SendSMS().Result;

            Console.WriteLine("Hello World!");
        }
    }

    public class TestClass
    {
        public async Task<SendSMSResponse> SendSMS()
        {
            ServiceCaller caller = new ServiceCaller();
            var response = await caller.SendSmsAsync();
            return response;
        }
    }
}
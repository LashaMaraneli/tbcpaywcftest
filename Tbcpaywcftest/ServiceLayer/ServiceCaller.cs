﻿using SmsService;
using System;
using System.Threading.Tasks;

namespace ServiceLayer
{
    public class ServiceCaller
    {
        SmsService.SmscallcenterserviceClient.EndpointConfiguration endpoint;
        string endpointAddress;

        public ServiceCaller()
        {
            endpoint = new SmsService.SmscallcenterserviceClient.EndpointConfiguration();
            endpointAddress = "http://sms.tbcpay.ge/Smscallcenterservice.svc";
        }

        public async Task<SendSMSResponse> SendSmsAsync()
        {
            var service = new SmsService.SmscallcenterserviceClient(endpoint, endpointAddress);
            SendSMSResponse smsResponse = null;
            try
            {
                var smsRequest = new SendSMSRequest("admin", "tbcpay", "514142211", "GAYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY", string.Empty, true, ServiceID.tbcpayge);
                smsResponse = await service.SendSMSAsync(smsRequest);
            }
            catch(Exception ex)
            {
            }
            return smsResponse;
        }
    }
}
